package Services;

import data.structures.Record;
import data.structures.RecordList;
import data.structures.Player;
import database.ConnectionManager;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import json.JsonBuilder;

/**
 * REST Web Service
 *
 * @author diazj
 * @author TheCheeseman
 */
@Path("Accounts")
public class AccountsResource {
    private static ConnectionManager connManager = new ConnectionManager();
    private static Map<Integer, Player> users = new HashMap();
    
    @GET
    public String logout(@QueryParam("id") Integer id) {
        users.remove(id);
        JsonBuilder response = new JsonBuilder();
        response.add("success", true);
        response.add("message", "Sesión finalizada.");
        return response.build();
    }
    
    @POST
    public String login(
        @FormParam("Username") String uname,
        @FormParam("Password") String pass
    ) {
        
        RecordList result = connManager.queryXML("postgresql", "getUser", uname, pass);
        Record userRecord = result.get(0);
        
        JsonBuilder response = new JsonBuilder();
        
        if (userRecord != null) {
            Integer id = userRecord.getInteger("id");
            Integer level = userRecord.getInteger("level");
            Integer experience = userRecord.getInteger("experience");
            String name = userRecord.getString("name");
            
            if (users.containsKey(id)) {
                response.add("success", false);
                response.add("message", "Este usuario ya se encuentra connectado.");
            } else {
                response.add("success", true);
                response.add("name", name);
                response.add("id", id);
                response.add("level", level);
                response.add("experience", experience);
                
                Player userObj = new Player(id, name, level, experience);
                users.put(id, userObj);
            }
            
            return response.build();
        } else {
            response.add("success", false);
            response.add("message", "Inicio de sesión fallido. Verifique sus credenciales.");
            return response.build();
        }
    }

    @PUT
    public String register(
        @QueryParam("Username") String username,
        @QueryParam("Password") String pass,
        @QueryParam("Email") String email,
        @QueryParam("Name") String name
    ) {
        boolean success = connManager.statementXML("postgresql", "registerUser", 
                username, pass, email, name);
        
        JsonBuilder response = new JsonBuilder();
        
        if (success) {
            response.add("success", true);
            response.add("message", "Usuario creado exitosamente.");
        } else {
            response.add("success", false);
            response.add("message", "El usuario no pudo ser creado.");
        }
        
        return response.build();
    }
}
