package Services;

import data.structures.Game;
import data.structures.Player;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import json.JsonBuilder;

/**
 * REST Web Service
 *
 * @author diazj
 * @author TheCheeseman
 */
@Path("Games")
public class GamesResource {
    private static final Integer MAX_ROOMS = 10;
    private static Map<Integer, Game> gameList = new HashMap();
    
    @POST
    @Path("Search")
    public String searchGame(
        @FormParam("id") String id, 
        @FormParam("name") String name,
        @FormParam("level") String level,
        @FormParam("experience") String experience
    ){
        JsonBuilder response = new JsonBuilder();
        Player player = new Player(Integer.parseInt(id), name, Integer.parseInt(level), Integer.parseInt(experience));
        boolean joined = false;
        
        Set roomIDs = gameList.keySet();
        Iterator iter = roomIDs.iterator();
        
        while (iter.hasNext()) {
            Integer roomKey = (Integer) iter.next();
            
            Game roomToJoin = gameList.getOrDefault(roomKey, null);
            System.out.println("Checking room " + roomKey);
            if (roomToJoin == null || roomToJoin.CLOSED) {
                System.out.println("Room " + roomKey + " is no longer valid or is closed."
                        + "Room will be unlisted.");
                gameList.remove(roomKey);
                continue;
            }
            
            joined = roomToJoin.join(player);
            if (joined) {
                response.add("address", roomToJoin.getServerAddress());
                response.add("roomID", roomToJoin.getGameId());
                System.out.println("Joined queue for available room " + roomToJoin.getGameId());
                break;
            }
        }
        
        if (joined == false) {
            Integer generatedKey = ((Number) Math.rint(Math.random() * MAX_ROOMS)).intValue();
            while (roomIDs.contains(generatedKey)) {
                generatedKey = ((Number) Math.rint(Math.random() * MAX_ROOMS)).intValue();
            }
            
            Game openedRoom = new Game(generatedKey, player);
            gameList.put(generatedKey, openedRoom);
            
            joined = true;
            response.add("address", openedRoom.getServerAddress());
            response.add("roomID", openedRoom.getGameId());
            System.out.println("Joined queue for created room " + openedRoom.getGameId());
        }
        
        response.add("success", joined);
        return response.build();
    }
    
    @GET
    @Path("Ready")
    public String checkRoomReady(@QueryParam("roomID") String id) {
        JsonBuilder response = new JsonBuilder();
        
        Game room = gameList.get(Integer.parseInt(id));
        boolean success = room.ready();
        
        if (success) {
            response.add("roomData", room.toJSON());
        }
        
        response.add("success", success);
        return response.build();
    }
    
    @GET
    @Path("Leave")
    public String leaveQueue (
        @QueryParam("roomID") String roomID,
        @QueryParam("playerID") String playerID
    ) {
        JsonBuilder response = new JsonBuilder();
        boolean success = false;
        
        Game room = gameList.getOrDefault(Integer.parseInt(roomID), null);
        if (room != null) {
            room.leave(Integer.parseInt(playerID));
            success = true;
        } else {
            response.add("message", "ID de sala de juego inválido.");
        }
        
        response.add("success", success);
        return response.build();
    }
    
    @GET
    @Path("Update")
    public String updateLobby (
        @QueryParam("roomID") String roomID,
        @QueryParam("playerID") String playerID,
        @QueryParam("championID") String championID,
        @QueryParam("championName") String championName
    ) {
        JsonBuilder response = new JsonBuilder();
        boolean success = false;
        
        Game room = gameList.getOrDefault(Integer.parseInt(roomID), null);
        if (room != null) {
            Player player = room.getPlayerById(Integer.parseInt(playerID));
            player.setChampionID(Integer.parseInt(championID));
            player.setChampionName(championName);
            
            response.add("roomData", room.toJSON());
            success = true;
        } else {
            response.add("message", "ID de sala de juego inválido.");
        }
        
        response.add("success", success);
        return response.build();
    }
}
