package Services;

import data.structures.Record;
import data.structures.RecordList;
import database.ConnectionManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import json.JsonBuilder;

/**
 * REST Web Service
 *
 * @author diazj
 * @author TheCheeseman
 */
@Path("Statistics")
public class StatisticsResource {
    private JsonBuilder response = new JsonBuilder();
    private ConnectionManager manager = new ConnectionManager();
    
    @GET
    public String retrieveStatistics(
        @QueryParam("playerID") String playerID
    ) {
        RecordList result = manager.queryXML("postgresql", "retrieveStatistics", Integer.parseInt(playerID));
        Record row = result.get(0);
        
        if (row != null) {
            response.add("wins", row.getInteger("wins")); 
            response.add("losses", row.getInteger("losses")); 
            response.add("kills", row.getInteger("kills")); 
            response.add("deaths", row.getInteger("deaths"));
            response.add("underling_kills", row.getInteger("underling_kills")); 
            response.add("turret_kills", row.getInteger("turret_kills"));
            
            response.add("nextLevel", row.getInteger("nextLevel"));
            response.add("requiredExperience", row.getInteger("requiredExperience"));
            response.add("playerLevel", row.getInteger("playerLevel"));
            response.add("playerExperience", row.getInteger("playerExperience"));
            
            response.add("success", true);
        } else {
            response.add("message", "Error: ID de jugador inválido. Informe al administrador.");
            response.add("success", false);
        }
        
        return response.build();
    }

    @PUT
    public String updateStatistics(
        @QueryParam("playerID") String playerID,
        @QueryParam("matchWon") boolean matchWon,
        @QueryParam("kills") String kills,
        @QueryParam("deaths") String deaths,
        @QueryParam("underlingKills") String underlingKills,
        @QueryParam("turretKills") String turretKills
    ) {
        Integer championKills = Integer.parseInt(kills);
        Integer underKills = Integer.parseInt(underlingKills);
        Integer turrKills = Integer.parseInt(turretKills);
            
        Integer acquiredExperience = 0;
        
        if (matchWon) {
            acquiredExperience += 15;
        } else {
            acquiredExperience += 10;
        }
        
        Integer pDeaths = Integer.parseInt(deaths);
        if (pDeaths == 0) {
            acquiredExperience += championKills * 5;
        } else {
            acquiredExperience += Math.round(championKills / pDeaths) * 5;
        }
        
        acquiredExperience += Math.round(underKills / 4);
        acquiredExperience += turrKills * 5;
        
        //Max ammount of acquiredExperience won per match
        if (acquiredExperience > 100) {
            acquiredExperience = 100;
        }
        
        Integer id = Integer.parseInt(playerID);
        RecordList result = manager.queryXML("postgresql", "retrieveStatistics", id);
        
        if (result.size() > 0) {
            Record row = result.get(0);
            Integer playerLevel = row.getInteger("currentLevel");
            Integer nextLevel = row.getInteger("nextLevel");
            Integer currentExperience = row.getInteger("currentExperience");
            Integer requiredExperience = row.getInteger("requiredExperience");
            
            currentExperience += acquiredExperience;
            if (currentExperience > requiredExperience) {
                playerLevel = nextLevel;
            }
            
            manager.statementXML("postgresql", "updatePlayerProgress", 
                    playerLevel, currentExperience, id);
            
            Integer wonMatches = row.getInteger("wins");
            Integer lostMatches = row.getInteger("losses");
            if (matchWon) {
                wonMatches++;
            } else {
                lostMatches++;
            }
            
            championKills += row.getInteger("kills");
            underKills += row.getInteger("underling_kills");
            turrKills += row.getInteger("turret_kills");
            pDeaths += row.getInteger("deaths");
            
            manager.statementXML("postgresql", "updateStatistics", 
                wonMatches, lostMatches, championKills, pDeaths, underKills,
                turrKills, id
            );
            
            response.add("success", true);
            response.add("Estadísticas actualizadas.");
        } else {
            response.add("success", false);
            response.add("Error durante la actualización de estadísticas. Informe al administrador.");
        }
        
        return response.build();
    }
}
