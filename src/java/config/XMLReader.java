package config;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author diazj
 */
public class XMLReader {
    private File xml;
    private Document doc;
    
    public XMLReader(String path) {
        try {
            URL filePath = new URL(this.getClass().getResource(path).toString().replace("%20", " "));
            xml = new File(filePath.getPath());
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xml);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String get(String ... nodeNames) {
        Node root = doc.getFirstChild();
        return searchNode(root, 0, nodeNames);
    }
    
    private String searchNode(Node node, Integer level, String ... nodeNames) {
        String result = null;
        
        NodeList childrenNodes = node.getChildNodes();
        
        for (int i = 0; i < childrenNodes.getLength(); i++) {
            Node child = childrenNodes.item(i);
            if (child.getNodeName().equals(nodeNames[level])) {
                if (level == nodeNames.length - 1) {
                    result = child.getTextContent();
                } else {
                    result = searchNode(child, level + 1, nodeNames);
                }
                
                break;
            }
        }
        
        return result;
    }
}
