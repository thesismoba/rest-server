package data.structures;

import config.XMLReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import json.JsonBuilder;

/**
 *
 * @author diazj
 * @author TheCheeseman
 */
public class Game implements JSONSerialization {
    //For testing purposes this will be 2 players by default
    private static Integer maxPlayers = 2;
    
    private static Integer levelVariation = 2;
    private static Integer gameID = 0;
    
    private Integer baseLevel;
    private Integer minLevel;
    private Integer maxLevel;
    
    //Currently not used
    private Float averageLevel;
    
    private Integer currentPlayers;
    private Map<Integer, Player> players;
    
    private static Integer basePort = 7780;
    private String serverAddress;
    private Process serverInstance;
    
    public boolean OPEN = true;
    public boolean CLOSED = false;
    public boolean PLAYING = false;
    public boolean GAME_OVER = false;
    
    public Game(Integer roomID, Player firstPlayer) {
        players = new HashMap();
        
        baseLevel = firstPlayer.getLevel();
        averageLevel = baseLevel/1.0f;
        minLevel = baseLevel - levelVariation;
        maxLevel = baseLevel + levelVariation;
        currentPlayers = 1;
        players.put(currentPlayers, firstPlayer);
        
        gameID = roomID;
    }
    
    public String getServerAddress() {
        return serverAddress;
    }
    
    public Integer getGameId() {
        return gameID;
    }
    
    public boolean join(Player player) {
        if (player.getLevel() > minLevel && player.getLevel() < maxLevel && currentPlayers < maxPlayers) {
            currentPlayers = players.size() + 1;
            players.put(currentPlayers, player);
            
            System.out.println("Player " + player.getName() + " has joined match " + gameID);
            return true;
        }
        
        return false;
    }
    
    public void leave(Integer playerID) {
        players.remove(playerID);
        currentPlayers = players.size();
        
        if (currentPlayers == 0) {
            this.CLOSED = true;
            finished();
        }
    }
    
    public boolean ready() {
        boolean maxPlayersJoined = Objects.equals(currentPlayers, maxPlayers);
        
        if (maxPlayersJoined && this.OPEN == true && this.PLAYING == false) {
            this.OPEN = false;
            this.PLAYING = true;
            
            try{    
                XMLReader xmlr = new XMLReader("DedicatedServerConfiguration.xml");
                String execPath = xmlr.get("Path");
                
                Integer port = basePort + gameID;
                Runtime rt = Runtime.getRuntime();
                
                //Run dedicated server listening to the specified port and 
                //Show console for monitoring server errors adn messages
                serverInstance = rt.exec("start " + execPath + "/Game/Maps/5v5 port=" + port + " -log"); 
                
                //Wait for the server to load before sending response
                serverInstance.waitFor(15, TimeUnit.SECONDS);

                //Address that players will connect to
                serverAddress = xmlr.get("Address") + ":" + port;
            }catch( IOException | InterruptedException ex ){
                serverAddress = null;
                System.err.println("Server instance could not be created. Exception: " + ex);
            }
        }
        
        return maxPlayersJoined;
    }
    
    public void finished() {
        this.PLAYING = false;
        this.GAME_OVER = true;
        
        if (this.CLOSED && serverInstance.isAlive()) {
            serverInstance.destroy();
        }
    }
    
    public Player getPlayerById(Integer id) {
        Iterator iter = players.keySet().iterator();
        
        while (iter.hasNext()) {
            Integer playerNumber = (Integer) iter.next();
            Player player = players.get(playerNumber);
            if (Objects.equals(id, player.getId())) {
                return player;
            }
        }
        
        return null;
    }

    @Override
    public String toJSON() {
        JsonBuilder serializedGame = new JsonBuilder();
        JsonBuilder team1 = new JsonBuilder();
        JsonBuilder team2 = new JsonBuilder();
        
        for (int i = 1; i < currentPlayers; i += 2) {
            team1.add("player_" + i, players.get(i).toJSON());
        }
        
        for (int i = 2; i < currentPlayers; i += 2) {
            team2.add("player_" + i, players.get(i).toJSON());
        }
        
        serializedGame.add("roomID", gameID);
        serializedGame.add("address", serverAddress);
        serializedGame.add("team1", team1.build());
        serializedGame.add("team2", team2.build());
        return serializedGame.build();
    }

    public String toString(String propertyName) {
        return toJSON();
    }
}
