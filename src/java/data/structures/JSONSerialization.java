package data.structures;

/**
 *
 * @author diazj
 */
public interface JSONSerialization {
    
    /**
     * Converts this object into it's JSON representation for communication
     * with the client.
     * 
     * @return this Object's JSON representation.
     */
    public String toJSON();
}
