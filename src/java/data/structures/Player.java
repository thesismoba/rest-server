package data.structures;
import json.JsonBuilder;

/**
 *
 * @author diazj
 * @author TheCheeseman
 */
public class Player implements JSONSerialization {
    private Integer id;
    private String name;
    private Integer level;
    private Integer experience;
    private Integer championID;
    private String championName;

    public Player(Integer id, String name, Integer level, Integer experience) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.experience = experience;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getChampionID() {
        return championID;
    }

    public void setChampionID(Integer championID) {
        this.championID = championID;
    }

    public String getChampionName() {
        return championName;
    }

    public void setChampionName(String championName) {
        this.championName = championName;
    }
    
    @Override
    public String toJSON() {
        JsonBuilder serializer = new JsonBuilder();
        
        serializer.add("id", id);
        serializer.add("name", name);
        serializer.add("level", level);
        
        return serializer.build();
    }

    public String toString(String propertyName) {
        return toJSON();
    }
}
