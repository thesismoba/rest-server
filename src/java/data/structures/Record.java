package data.structures;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import json.JsonBuilder;

/**
 * Custom data structure representing a database table row.
 * 
 * @author diazj
 */
public class Record implements JSONSerialization {
    private Map data;
    
    public Record () {
        data = new HashMap();
    }
    
    /**
     * Initializes this record with the provided map.
     * 
     * @param record 
     */
    public Record (Map record) {
        data = record;
    }
    
    public void add(String field, Object value) {
        data.put(field, value);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Object get (String field) {
        return data.getOrDefault(field, null);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Object get (String field, Object defaultValue) {
        return data.getOrDefault(field, defaultValue);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Integer getInteger (String field) {
        Integer intValue = ((Number) data.getOrDefault(field, null)).intValue();
        data.put(field, intValue);
        return intValue;
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Integer getInteger (String field, Object defaultValue) {
        Integer intValue = ((Number) data.getOrDefault(field, defaultValue)).intValue();
        data.put(field, intValue);
        return intValue;
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Float getFloat (String field) {
        return ((Number) data.getOrDefault(field, null)).floatValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Float getFloat (String field, Object defaultValue) {
        return ((Number) data.getOrDefault(field, defaultValue)).floatValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Double getDouble (String field) {
        return (Double) data.getOrDefault(field, null);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Double getDouble (String field, Object defaultValue) {
        return (Double) data.getOrDefault(field, defaultValue);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Short getShort (String field) {
        return ((Number) data.getOrDefault(field, null)).shortValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Short getShort (String field, Object defaultValue) {
        return ((Number) data.getOrDefault(field, defaultValue)).shortValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Long getLong (String field) {
        return ((Number) data.getOrDefault(field, null)).longValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public Long getLong (String field, Object defaultValue) {
        return ((Number) data.getOrDefault(field, defaultValue)).longValue();
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public String getString (String field) {
        return (String) data.getOrDefault(field, null);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public String getString (String field, Object defaultValue) {
        return (String) data.getOrDefault(field, defaultValue);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public char getChar (String field) {
        return (char) data.getOrDefault(field, null);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public char getChar (String field, Object defaultValue) {
        return (char) data.getOrDefault(field, defaultValue);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public boolean getBool (String field) {
        return (boolean) data.getOrDefault(field, null);
    }
    
    /**
     * Gets the value stored by the provided field name.
     * 
     * @param field
     * @param defaultValue Value to be returned if the field is not present.
     * @return value The value stored or <code>null</code> if the specified
     * field is not present.
     */
    public boolean getBool (String field, Object defaultValue) {
        return (boolean) data.getOrDefault(field, defaultValue);
    }
    
    /**
     * Add a new field or modify a field's value in the record.
     * 
     * @param fieldName Field name to create of modify
     * @param value Value to be stored
     */
    public void set (String fieldName, Object value) {
        data.put(fieldName, value);
    }

    @Override
    public String toJSON() {
        JsonBuilder builder = new JsonBuilder();
        
        Set fields = data.keySet();
        for (Object key : fields) {
            System.out.println(key + ": " + this.get((String) key));
            builder.add((String) key, this.get((String) key));
        }
        
        return builder.build();
    }
    
    @Override
    public String toString() {
        return toJSON();
    }
}
