package data.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import json.JsonBuilder;

/**
 * Custom data structure representing a database table or just a series of rows.
 * 
 * @author diazj
 */
public class RecordList implements JSONSerialization{
    private Map<Object, Record> list;
    
    public RecordList() {
        list = new HashMap();
    }
    
    /**
     * Initializes and fills the list with records sent from the client.
     * These records are maps representing JSON records.
     * 
     * @param records 
     */
    public RecordList (ArrayList records) {
        list = new HashMap();
        Iterator iter = records.iterator();
        
        while (iter.hasNext()) {
            this.add(new Record((Map) iter.next()));
        }
    }
    
    /**
     * Initializes and fills the list with the provided records. The index is 
     * assigned based on the order of the input <code>records</code> starting 
     * from 0.
     * 
     * @param records 
     */
    public RecordList (Record ... records) {
        list = new HashMap();
        
        for (Record record : records) {
            this.add(record);
        }
    }
    
    /**
     * Adds a record at the end of the list.
     * 
     * @param record 
     */
    public final void add (Record record) {
        list.put(list.size(), record);
    }
    
    /**
     * Adds a record or updates the record at the specified index.
     * 
     * @param index
     * @param record 
     */
    public final void add (Integer index, Record record) {
        list.put(index, record);
    }
    
    /**
     * Get the record at the specified index.
     * 
     * @param index
     * @return 
     */
    public Record get (Integer index) {
        return list.getOrDefault(index, null);
    }
    
    public Integer size() {
        return list.size();
    }

    @Override
    public String toJSON() {
        return toJSON("recordList");
    }
    
    public String toJSON(String propertyName) {
        JsonBuilder builder = new JsonBuilder();
        
        Object[] jsons = new Object[list.size()];
        Integer i = 0;
        
        Set indexes = list.keySet();
        for (Object index : indexes) {
            jsons[i] = list.get(index).toJSON();
            i++;
        }
        
        builder.add(propertyName, jsons);
        return builder.build();
    }
    
    @Override
    public String toString() {
        return toJSON();
    }

}
