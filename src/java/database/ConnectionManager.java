package database;

import config.XMLReader;
import data.structures.Record;
import data.structures.RecordList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author diazj
 */
public class ConnectionManager {
    private static final ConnectionPool pool = new ConnectionPool("pgsql");
    private HttpSession request;
    
    public RecordList query (String connectionName, String query, Object ... parameters) {
        Connection conn = pool.getConnection(connectionName);
        
        try {
            PreparedStatement pstm = conn.prepareStatement(query);
            
            for (int i = 1; i <= parameters.length; i++) {
                pstm.setObject(i, parameters[i-1]);
            }
            
            ResultSet queryResult = pstm.executeQuery();
            int columns = queryResult.getMetaData().getColumnCount() + 1;
            RecordList result = new RecordList();
            
            while (queryResult.next()) {
                Record rec = new Record();
                for (int i = 1; i < columns; i++) {
                    rec.add(queryResult.getMetaData().getColumnName(i), queryResult.getObject(i));
                }
                result.add(rec);
            }
            
            conn.commit();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            pool.returnConnection(connectionName, conn);
        }
    }
    
    public RecordList queryXML (String connectionName, String queryName, Object ... parameters) {
        XMLReader xmlr = new XMLReader("Querys.xml");
        return query(connectionName, xmlr.get(queryName), parameters);
    }
    
    public boolean statement (String connectionName, String query, Object ... parameters) {
        Connection conn = pool.getConnection(connectionName);
        
        try {
            PreparedStatement pstm = conn.prepareStatement(query);
            
            for (int i = 1; i <= parameters.length; i++) {
                pstm.setObject(i, parameters[i-1]);
            }
            
            pstm.execute();
            
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return false;
        } finally {
            pool.returnConnection(connectionName, conn);
        }
    }
    
    public boolean statementXML (String connectionName, String queryName, Object ... parameters) {
        XMLReader xmlr = new XMLReader("Querys.xml");
        return statement(connectionName, xmlr.get(queryName), parameters);
    }
}
