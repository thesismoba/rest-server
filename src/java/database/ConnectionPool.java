package database;

import config.XMLReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author diazj
 */
public class ConnectionPool {
    private XMLReader xmlr;
    private Map<String, Connection[]> connectionList;
    
    private Integer tries = 3;
    private long waitTime = 1000;
    
    public ConnectionPool(String ... drivers) {
        connectionList = new HashMap();
        xmlr = new XMLReader("ConnectionProperties.xml");
        int foundDrivers = 0;
        
        try {
            for (String driver : drivers) {
                System.out.println("Loading driver: " + driver);
                String driverClassName = xmlr.get(driver, "className");
                Class.forName(driverClassName);
                openConnection(driver);
                foundDrivers++;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ConnectionPool.class.getName()).log(Level.WARNING, null, ex);
        }
        
        if (foundDrivers == 0) {
            Logger.getLogger(ConnectionPool.class.getName()).log(Level.SEVERE, null, "No drivers were found.");
        }
    }
    
    private void openConnection(String driver) throws SQLException {
        int minConns = Integer.parseInt(xmlr.get(driver, "connections", "min"));
        int maxConns = Integer.parseInt(xmlr.get(driver, "connections", "max"));
        
        Connection[] conns = new Connection[minConns];
        
        for (int i = 0; i < minConns; i++) {
            String driverString = 
                    xmlr.get(driver, "driverString") + 
                    xmlr.get(driver, "host") + ":" + 
                    xmlr.get(driver, "port");
            if (driver.equals("oracle")) {
                driverString += ":xe";
            } else if (driver.equals("mssql")) {
                driverString += xmlr.get(driver, "databaseName");
            } else {
                driverString += "/" + xmlr.get(driver, "databaseName");
            }
            
            System.out.println(driverString);
            
            conns[i] = DriverManager.getConnection(driverString, xmlr.get(driver, "username"), xmlr.get(driver, "password"));
            conns[i].setAutoCommit(false);
        }
        
        connectionList.put(xmlr.get(driver, "name"), conns);
    }
    
    public Connection getConnection(String connectionName) {
        return getConnectionWithTries(connectionName, tries - 1);
    }
    
    public Connection getConnectionWithTries(String connectionName, Integer remainingTries) {
        Connection[] connArray = connectionList.getOrDefault(connectionName, null);
        Connection requestedConnection = null;
        
        for (Integer i = 0; connArray != null && i < connArray.length && remainingTries > 0; i++) {
            requestedConnection = connArray[i];
            
            if (requestedConnection != null) {
                connArray[i] = null;
                break;
            }
        }
        
        if (requestedConnection == null) {
            try {
                Thread.sleep(waitTime);
                requestedConnection = getConnectionWithTries(connectionName, remainingTries - 1);
            } catch (InterruptedException ex) {
                Logger.getLogger(ConnectionPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return requestedConnection;
    }
    
    public void returnConnection(String connectionName, Connection usedConnection) {
        Connection[] connArray = connectionList.getOrDefault(connectionName, null);
        
        if (connArray != null) {
            for (Integer i = 0; i < connArray.length; i++) {
                if (connArray[i] == null) {
                    connArray[i] = usedConnection;
                    break;
                }
            }
        }
    }
}
