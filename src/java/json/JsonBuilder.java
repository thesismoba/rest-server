package json;

public class JsonBuilder{
    private String json = "";
    
    /**
     * Adds a property with the input name and given value to the JSON
     * string.
     * 
     * @param name
     * @param value 
     */
    public void add(String name, Object value) {
        if (!json.isEmpty()) {
            json += ",";
        }
        
        json += "\"" + name + "\": ";
        
        if(value instanceof Number || value instanceof Boolean) {
            json += value;
        } else if (value instanceof String) {
            String str = (String) value;
            if (str.startsWith("{") && str.endsWith("}")) {
                json += value;
            } else {
                json += "\"" + value + "\"";
            }
        } else if (value instanceof JsonBuilder) {
            json += ((JsonBuilder) value).build();
        } else {
            json += "\"" + value + "\"";
        }
    }
    
    /**
     * Adds an array of values under the given property name.
     * 
     * @param name
     * @param array 
     */
    public void add(String name, Object ... array) {
        if (!json.isEmpty()) {
            json += ",";
        }
        
        json += "\"" + name + "\": [";
        String valueString = "[";
        
        boolean first = true;
        for (Object obj: array) {
            if (!first) {
                json += ",";
            } else {
                first = false;
            }
            
            if (obj instanceof Number || obj instanceof Boolean) {
                json += obj;
            } else if (obj instanceof String) {
                String str = (String) obj;
                if (str.startsWith("{") && str.endsWith("}")) {
                    json += obj;
                } else {
                    json += "\"" + obj + "\"";
                }
            } else if (obj instanceof JsonBuilder) {
                json += ((JsonBuilder) obj).build();
            } else {
                json += "\"" + obj + "\"";
            }
        }
        
        json += "]";
    }
    
    /**
     * Adds a 2-dimensional array under the given property name.
     * 
     * @param name
     * @param matrix 
     */
    public void add(String name, Object[][] matrix) {
        if (!json.isEmpty()) {
            json += ",";
        }
        
        json += "\"" + name + "\": [";
        
        boolean firstArray = true;
        boolean firstValue;
        for (Object[] array: matrix) {
            if (!firstArray) {
                json += ",";
            }
            firstArray = false;
            firstValue = true;
            json += "[";
            for (Object obj: array) {
                if (!firstValue) {
                    json += ",";
                    
                }
                firstValue = false;
                if (obj instanceof Number) {
                    json += obj;
                } else if (obj instanceof JsonBuilder) {
                    json += ((JsonBuilder) obj).build();
                } else {
                    json += "\"" + obj + "\"";
                }
            }
            json += "]";
        }
        json += "]";
    }
    
    /**
     * Clears all added properties from this JsonBuilder.
     */
    public void clear(){
        json = "";
    }
    
    /**
     * Builds a JSON string parse-able by the client.
     * 
     * @return JSON string
     */
    public String build(){
        if (json.isEmpty()) {
            return "{}";
        } else {
            return "{" + json + "}";
        }   
    }
}